use tokio_actors::{Context, Error};

async fn actor(_: &mut (), _msg: (), context: &mut Context<()>) -> Result<(), Error> {
    tracing::info!("Only invocation");
    context.stop();
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    init_subscriber()?;

    let mut root = tokio_actors::root();

    let sender = root.spawn_child((), actor).await?;

    // attempt to send 32 messages
    for i in 0..32 {
        // expect this send to start failing at some point, we may be able to buffer a number of
        // messages to the actor before it processes one
        if sender.send(()).await.is_ok() {
            tracing::info!("Sent message #{}", i);
        } else {
            tracing::info!("Failed to send message #{}", i);
        }
    }

    root.close().await;

    Ok(())
}

fn init_subscriber() -> Result<(), Error> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::{layer::SubscriberExt, EnvFilter, Registry};

    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));
    let formatter = tracing_subscriber::fmt::layer().pretty();

    let subscriber = Registry::default()
        .with(ErrorLayer::default())
        .with(env_filter)
        .with(formatter);

    tracing::subscriber::set_global_default(subscriber)?;

    Ok(())
}
