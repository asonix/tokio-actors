use tokio_actors::{Context, Error};

#[derive(Debug, thiserror::Error)]
#[error("Recoverable error")]
struct RecoverableError;

#[derive(Debug, thiserror::Error)]
#[error("Terrible error")]
struct TerribleError;

// this actor will continue processing messages, even after erroring
async fn actor(_: &mut (), _msg: (), _: &mut Context<()>) -> Result<(), Error> {
    tracing::info!("Repeating invocation");
    Err(RecoverableError.into())
}

// this actor will stop processing messages after erroring once
async fn stopping_actor(_: &mut (), _: (), context: &mut Context<()>) -> Result<(), Error> {
    tracing::info!("Only invocation");
    context.stop_on_error();
    Err(TerribleError.into())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    init_subscriber()?;

    let mut root = tokio_actors::root();

    let sender1 = root.spawn_child((), actor).await?;
    let sender2 = root.spawn_child((), stopping_actor).await?;

    // attempt to send 32 messages
    for i in 0..32 {
        // expect this send to always succeed
        if sender1.send(()).await.is_ok() {
            tracing::info!("Sent message #{} to actor", i);
        } else {
            tracing::info!("Failed to send message #{} to actor", i);
        }

        // expect this send to fail at some point. The receiver will be dropped after the actor
        // processes the first message, but we might buffer many messages before then
        if sender2.send(()).await.is_ok() {
            tracing::info!("Sent message #{} to stopping actor", i);
        } else {
            tracing::info!("Failed to send message #{} to stopping actor", i);
        }
    }

    root.close().await;

    Ok(())
}

fn init_subscriber() -> Result<(), Error> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::{layer::SubscriberExt, EnvFilter, Registry};

    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));
    let formatter = tracing_subscriber::fmt::layer().pretty();

    let subscriber = Registry::default()
        .with(ErrorLayer::default())
        .with(env_filter)
        .with(formatter);

    tracing::subscriber::set_global_default(subscriber)?;

    Ok(())
}
