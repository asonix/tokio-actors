use tokio_actors::{Context, Error};

async fn inner_actor(state: &mut usize, _msg: (), context: &mut Context<()>) -> Result<(), Error> {
    tracing::info!(
        "Hello from inner actor {}, current state: {}",
        context.actor_id(),
        *state
    );
    *state -= 1;

    if *state != 0 {
        // Send a message to ourself
        context.send(()).await?;
    }

    Ok(())
}

async fn outer_actor(state: &mut usize, _msg: (), context: &mut Context<()>) -> Result<(), Error> {
    tracing::info!(
        "Hello from outer actor {}, current state: {}",
        context.actor_id(),
        *state
    );
    *state += 1;

    let sender = context.spawn_child(*state, inner_actor);

    // send a message to the newly spawned child
    sender.send(()).await?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    init_subscriber()?;

    // Create the root actor
    let mut root = tokio_actors::root();

    for _ in 0..4 {
        let sender = root.spawn_child(0, outer_actor).await?;

        for _ in 0..4 {
            // Send a message to our newly spawned child
            sender.send(()).await?;
        }
    }

    tokio::signal::ctrl_c().await?;
    // Signal all actors to close. Actors will automatically close if root is dropped, but
    // deliberally calling close will allow the actors to close gracefully before the program stops
    root.close().await;

    Ok(())
}

fn init_subscriber() -> Result<(), Error> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::{layer::SubscriberExt, EnvFilter, Registry};

    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));
    let formatter = tracing_subscriber::fmt::layer().pretty();

    let subscriber = Registry::default()
        .with(ErrorLayer::default())
        .with(env_filter)
        .with(formatter);

    tracing::subscriber::set_global_default(subscriber)?;

    Ok(())
}
