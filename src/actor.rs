use crate::{Context, Error, HookHandler, MessageHandler, RemoveHandler};
use std::{future::Future, marker::PhantomData, pin::Pin};

type BoxFuture<'a> = Pin<Box<dyn Future<Output = Result<(), Error>> + Send + 'a>>;

type HookDummy<State, Message> =
    for<'a> fn(&'a mut State, &'a mut Context<Message>) -> BoxFuture<'a>;
type RemoveDummy<State, Message> =
    for<'a> fn(&'a mut State, usize, &'a mut Context<Message>) -> BoxFuture<'a>;

#[derive(Debug)]
pub struct Actor<State, F, Start, Stop, Remove, Message> {
    pub(crate) state: State,
    pub(crate) function: F,
    pub(crate) on_start: Option<Start>,
    pub(crate) on_stop: Option<Stop>,
    pub(crate) on_remove: Option<Remove>,
    pub(crate) message: PhantomData<Message>,
}

impl<State, F, Message>
    Actor<
        State,
        F,
        HookDummy<State, Message>,
        HookDummy<State, Message>,
        RemoveDummy<State, Message>,
        Message,
    >
where
    for<'a> F: MessageHandler<'a, State, Message> + Send + Sync + 'static,
    State: Send + 'static,
    Message: Send + 'static,
{
    pub fn new(state: State, function: F) -> Self {
        Actor {
            state,
            function,
            on_start: None,
            on_stop: None,
            on_remove: None,
            message: PhantomData,
        }
    }
}

impl<State, F, Start, Stop, Remove, Message> Actor<State, F, Start, Stop, Remove, Message>
where
    for<'a> F: MessageHandler<'a, State, Message> + Send + Sync + 'static,
    for<'a> Start: HookHandler<'a, State, Message> + Send + Sync + 'static,
    for<'a> Stop: HookHandler<'a, State, Message> + Send + Sync + 'static,
    for<'a> Remove: RemoveHandler<'a, State, Message> + Send + Sync + 'static,
    State: Send + 'static,
    Message: Send + 'static,
{
    pub fn on_start<NewStart>(
        self,
        on_start: NewStart,
    ) -> Actor<State, F, NewStart, Stop, Remove, Message>
    where
        for<'a> NewStart: HookHandler<'a, State, Message> + Send + Sync + 'static,
    {
        Actor {
            state: self.state,
            function: self.function,
            on_start: Some(on_start),
            on_stop: self.on_stop,
            on_remove: self.on_remove,
            message: self.message,
        }
    }

    pub fn on_stop<NewStop>(
        self,
        on_stop: NewStop,
    ) -> Actor<State, F, Start, NewStop, Remove, Message>
    where
        for<'a> NewStop: HookHandler<'a, State, Message> + Send + Sync + 'static,
    {
        Actor {
            state: self.state,
            function: self.function,
            on_start: self.on_start,
            on_stop: Some(on_stop),
            on_remove: self.on_remove,
            message: self.message,
        }
    }

    pub fn on_remove<NewRemove>(
        self,
        on_remove: NewRemove,
    ) -> Actor<State, F, Start, Stop, NewRemove, Message>
    where
        for<'a> NewRemove: RemoveHandler<'a, State, Message> + Send + Sync + 'static,
    {
        Actor {
            state: self.state,
            function: self.function,
            on_start: self.on_start,
            on_stop: self.on_stop,
            on_remove: Some(on_remove),
            message: self.message,
        }
    }
}
