use crate::{Actor, Error, HookHandler, MessageHandler, RemoveHandler};
use once_cell::sync::Lazy;
use std::sync::Mutex;
use std::{
    collections::HashMap,
    future::Future,
    pin::Pin,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
    task::{Context as StdContext, Poll},
};
use tokio::{
    select,
    sync::oneshot::{Receiver, Sender},
};
use tracing::Instrument;

type ActorSender<M> = tokio::sync::mpsc::Sender<ActorMessage<M>>;
type ActorReceiver<M> = tokio::sync::mpsc::Receiver<ActorMessage<M>>;
type SendError<M> = tokio::sync::mpsc::error::SendError<ActorMessage<M>>;

static ACTOR_ID_GENERATOR: Lazy<Arc<AtomicUsize>> = Lazy::new(|| Arc::new(AtomicUsize::new(0)));

async fn root_turn<'a>(_: &'a mut (), _: (), _: &'a mut Context<()>) -> Result<(), Error> {
    Ok(())
}

pub fn root() -> Handle<()> {
    spawn(Actor::new((), root_turn))
}

#[tracing::instrument(name = "Spawning actor", skip(actor))]
fn spawn<State, F, Start, Stop, Remove, M>(
    actor: Actor<State, F, Start, Stop, Remove, M>,
) -> Handle<M>
where
    for<'a> F: MessageHandler<'a, State, M> + Send + Sync + 'static,
    for<'a> Start: HookHandler<'a, State, M> + Send + Sync + 'static,
    for<'a> Stop: HookHandler<'a, State, M> + Send + Sync + 'static,
    for<'a> Remove: RemoveHandler<'a, State, M> + Send + Sync + 'static,
    State: Send + 'static,
    M: Send + 'static,
{
    let (tx, rx) = tokio::sync::mpsc::channel(16);

    let (shutdown_notifier, shutdown) = ShutdownNotifier::new();
    let (close_notifier, close) = ShutdownNotifier::new();

    let id = ACTOR_ID_GENERATOR.fetch_add(1, Ordering::Relaxed);

    let mgr = FutureManager {
        tx: tx.clone(),
        rx,
        actor,
        shutdown,
        weak: shutdown_notifier.weak(),
        id,
        close_notifier,
    };

    tokio::spawn(mgr.run());

    Handle {
        tx,
        shutdown_notifier,
        actor_id: id,
        close,
    }
}

#[derive(Debug)]
pub enum ActorMessage<M> {
    Closed(usize),
    Spawn(CloseHandle),
    Msg(M),
}

#[derive(Clone, Debug)]
struct WeakShutdownNotifier(Arc<Mutex<Option<Sender<()>>>>);
#[derive(Debug)]
struct ShutdownNotifier(WeakShutdownNotifier);
#[derive(Debug)]
struct Shutdown(Receiver<()>);

#[derive(Debug)]
struct FutureManager<State, F, Start, Stop, Remove, M> {
    tx: ActorSender<M>,
    rx: ActorReceiver<M>,
    actor: Actor<State, F, Start, Stop, Remove, M>,
    shutdown: Shutdown,
    weak: WeakShutdownNotifier,
    id: usize,
    close_notifier: ShutdownNotifier,
}

#[derive(Debug)]
pub struct Handle<M> {
    tx: ActorSender<M>,
    shutdown_notifier: ShutdownNotifier,
    actor_id: usize,
    close: Shutdown,
}

#[derive(Clone, Debug)]
pub struct SendHandle<M> {
    tx: ActorSender<M>,
    weak: WeakShutdownNotifier,
    actor_id: usize,
}

pub struct CloseHandle {
    close_fut: Option<Pin<Box<dyn Future<Output = ()> + Send + Sync>>>,
    shutdown_notifier: ShutdownNotifier,
    actor_id: usize,
}

#[derive(Debug)]
pub struct Context<M> {
    tx: ActorSender<M>,
    should_stop: bool,
    stop_on_error: bool,
    children: HashMap<usize, CloseHandle>,
    actor_id: usize,
    weak: WeakShutdownNotifier,
}

impl WeakShutdownNotifier {
    fn shutdown(&mut self) {
        if let Some(sender) = self.0.lock().unwrap().take() {
            let _ = sender.send(());
        }
    }
}

impl ShutdownNotifier {
    fn new() -> (ShutdownNotifier, Shutdown) {
        let (tx, rx) = tokio::sync::oneshot::channel();
        let inner = WeakShutdownNotifier(Arc::new(Mutex::new(Some(tx))));
        (ShutdownNotifier(inner), Shutdown(rx))
    }

    fn shutdown(&mut self) {
        self.0.shutdown();
    }

    fn weak(&self) -> WeakShutdownNotifier {
        self.0.clone()
    }
}

impl<State, F, Start, Stop, Remove, M> FutureManager<State, F, Start, Stop, Remove, M> {
    #[tracing::instrument(skip(self))]
    async fn run(self)
    where
        for<'a> F: MessageHandler<'a, State, M>,
        for<'a> Start: HookHandler<'a, State, M>,
        for<'a> Stop: HookHandler<'a, State, M>,
        for<'a> Remove: RemoveHandler<'a, State, M>,
        M: 'static,
    {
        let FutureManager {
            tx,
            mut rx,
            actor:
                Actor {
                    mut state,
                    mut function,
                    on_start,
                    on_stop,
                    mut on_remove,
                    ..
                },
            mut shutdown,
            weak,
            id,
            mut close_notifier,
        } = self;

        tracing::debug!("Actor {} starting", id);

        let mut context = Context {
            tx,
            should_stop: false,
            stop_on_error: false,
            children: HashMap::new(),
            actor_id: id,
            weak,
        };

        let start_error = if let Some(on_start) = on_start {
            let span = tracing::info_span!(
                "On Start",
                actor.id = &tracing::field::display(&id),
                exception.message = tracing::field::Empty,
                exception.details = tracing::field::Empty
            );

            let res = span
                .in_scope(|| on_start.run(&mut state, &mut context))
                .instrument(span.clone())
                .await;

            if let Err(e) = &res {
                let display = format!("{}", e);
                let debug = format!("{:?}", e);
                span.record("exception.message", &display.as_str());
                span.record("exception.details", &debug.as_str());

                span.in_scope(|| {
                    tracing::error!("Failed to start actor: {}", e);
                    tracing::error!("details: {:?}", e);
                });
            }

            res.is_err()
        } else {
            false
        };

        if !start_error {
            loop {
                if context.should_stop {
                    break;
                }

                let turn_span = tracing::info_span!(
                    "Turning actor",
                    actor.id = &tracing::field::display(&id),
                    exception.message = tracing::field::Empty,
                    exception.details = tracing::field::Empty,
                );

                let fut = async {
                    if let Some(msg) = rx.recv().await {
                        match msg {
                            ActorMessage::Msg(msg) => {
                                Some(function.run(&mut state, msg, &mut context).await)
                            }
                            ActorMessage::Spawn(close_handle) => {
                                context.children.insert(close_handle.actor_id, close_handle);
                                Some(Ok(()))
                            }
                            ActorMessage::Closed(child_id) => {
                                tracing::debug!(
                                    "Actor {} received close notification for child actor {}",
                                    id,
                                    child_id
                                );
                                if let Some(mut close_handle) = context.children.remove(&child_id) {
                                    close_handle.close().await;
                                }
                                if let Some(ref mut on_remove) = &mut on_remove {
                                    Some(on_remove.run(&mut state, child_id, &mut context).await)
                                } else {
                                    Some(Ok(()))
                                }
                            }
                        }
                    } else {
                        None
                    }
                }
                .instrument(turn_span.clone());

                select! {
                    opt = fut => {
                        match opt {
                            Some(Err(e)) => {
                                let display = format!("{}", e);
                                let debug = format!("{:?}", e);
                                turn_span.record("exception.message", &display.as_str());
                                turn_span.record("exception.details", &debug.as_str());

                                turn_span.in_scope(|| {
                                    tracing::error!("Error in actor: {}", e);
                                    tracing::error!("details: {:?}", e);
                                });

                                if context.stop_on_error {
                                    break;
                                }
                            }
                            None => break,
                            _ => (),
                        }
                    },
                    _ = &mut shutdown => {
                        break;
                    }
                }
            }
        }

        if let Some(on_stop) = on_stop {
            let span = tracing::info_span!(
                "On Stop",
                exception.message = tracing::field::Empty,
                exception.details = tracing::field::Empty,
            );
            let res = span
                .in_scope(|| on_stop.run(&mut state, &mut context))
                .instrument(span.clone())
                .await;

            if let Err(e) = &res {
                let display = format!("{}", e);
                let debug = format!("{:?}", e);
                span.record("exception.message", &display.as_str());
                span.record("exception.details", &debug.as_str());

                span.in_scope(|| {
                    tracing::error!("Failed while stopping actor {}: {}", id, e);
                    tracing::error!("details: {:?}", e);
                });
            }
        }

        let futs = context
            .children
            .values_mut()
            .map(|child| child.close())
            .collect::<Vec<_>>();

        tracing::debug!("Waiting on Actor {}'s children", id);

        for fut in futs {
            fut.await;
        }

        tracing::debug!("Actor {} closing", id);

        close_notifier.shutdown();
    }
}

impl Future for Shutdown {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut StdContext<'_>) -> Poll<Self::Output> {
        Pin::new(&mut self.0).poll(cx).map(|_| ())
    }
}

impl Drop for ShutdownNotifier {
    fn drop(&mut self) {
        self.0.shutdown();
    }
}

impl<M> Handle<M> {
    #[tracing::instrument(skip(self, state, function))]
    pub async fn spawn_child<State, F, N>(
        &mut self,
        state: State,
        function: F,
    ) -> Result<SendHandle<N>, SendError<M>>
    where
        for<'a> F: MessageHandler<'a, State, N> + Send + Sync + 'static,
        State: Send + 'static,
        N: Send + 'static,
        M: Send + 'static,
    {
        self.spawn_child_with_hooks(Actor::new(state, function))
            .await
    }

    #[tracing::instrument(skip(self, actor))]
    pub async fn spawn_child_with_hooks<State, F, Start, Stop, Remove, N>(
        &mut self,
        actor: Actor<State, F, Start, Stop, Remove, N>,
    ) -> Result<SendHandle<N>, SendError<M>>
    where
        for<'a> F: MessageHandler<'a, State, N> + Send + Sync + 'static,
        for<'a> Start: HookHandler<'a, State, N> + Send + Sync + 'static,
        for<'a> Stop: HookHandler<'a, State, N> + Send + Sync + 'static,
        for<'a> Remove: RemoveHandler<'a, State, N> + Send + Sync + 'static,
        State: Send + 'static,
        N: Send + 'static,
        M: Send + 'static,
    {
        let handle = spawn(actor);
        let actor_id = handle.actor_id;
        let (send_handle, close_handle, close) = handle.split();
        let sender = self.tx.clone();
        tokio::spawn(async move {
            close.await;
            let _ = sender.send(ActorMessage::Closed(actor_id)).await;
        });
        self.tx.send(ActorMessage::Spawn(close_handle)).await?;
        Ok(send_handle)
    }

    fn split(self) -> (SendHandle<M>, CloseHandle, Shutdown)
    where
        M: Send + 'static,
    {
        let tx = self.tx.clone();
        let close_fut = Some(Box::pin(async move { tx.closed().await })
            as Pin<Box<dyn Future<Output = ()> + Send + Sync>>);

        (
            SendHandle {
                tx: self.tx,
                weak: self.shutdown_notifier.weak(),
                actor_id: self.actor_id,
            },
            CloseHandle {
                close_fut,
                shutdown_notifier: self.shutdown_notifier,
                actor_id: self.actor_id,
            },
            self.close,
        )
    }

    pub fn close<'a>(&'a mut self) -> impl Future<Output = ()> + 'a {
        self.shutdown_notifier.shutdown();

        async move {
            self.tx.closed().await;
        }
    }
}

impl<M> SendHandle<M> {
    pub async fn send(&self, message: M) -> Result<(), SendError<M>> {
        self.tx.send(ActorMessage::Msg(message)).await
    }

    pub fn stop(&mut self) {
        self.weak.shutdown();
    }

    pub fn actor_id(&self) -> usize {
        self.actor_id
    }

    #[tracing::instrument(skip(self, state, function))]
    pub async fn spawn_child<State, F, N>(
        &mut self,
        state: State,
        function: F,
    ) -> Result<SendHandle<N>, SendError<M>>
    where
        for<'a> F: MessageHandler<'a, State, N> + Send + Sync + 'static,
        State: Send + 'static,
        N: Send + 'static,
        M: Send + 'static,
    {
        self.spawn_child_with_hooks(Actor::new(state, function))
            .await
    }

    #[tracing::instrument(skip(self, actor))]
    pub async fn spawn_child_with_hooks<State, F, Start, Stop, Remove, N>(
        &mut self,
        actor: Actor<State, F, Start, Stop, Remove, N>,
    ) -> Result<SendHandle<N>, SendError<M>>
    where
        for<'a> F: MessageHandler<'a, State, N> + Send + Sync + 'static,
        for<'a> Start: HookHandler<'a, State, N> + Send + Sync + 'static,
        for<'a> Stop: HookHandler<'a, State, N> + Send + Sync + 'static,
        for<'a> Remove: RemoveHandler<'a, State, N> + Send + Sync + 'static,
        State: Send + 'static,
        N: Send + 'static,
        M: Send + 'static,
    {
        let handle = spawn(actor);
        let actor_id = handle.actor_id;
        let (send_handle, close_handle, close) = handle.split();
        let sender = self.tx.clone();
        tokio::spawn(async move {
            close.await;
            let _ = sender.send(ActorMessage::Closed(actor_id)).await;
        });
        self.tx.send(ActorMessage::Spawn(close_handle)).await?;
        Ok(send_handle)
    }

    pub fn every(&self, duration: std::time::Duration, f: impl Fn() -> M + Send + Sync + 'static)
    where
        M: Send + 'static,
    {
        let weak = SendHandle {
            tx: self.tx.clone(),
            weak: self.weak.clone(),
            actor_id: self.actor_id,
        };

        tokio::spawn(async move {
            let mut interval = tokio::time::interval(duration);

            interval.tick().await;
            while weak.send((f)()).await.is_ok() {
                interval.tick().await;
            }
        });
    }
}

impl CloseHandle {
    fn close<'a>(&'a mut self) -> impl Future<Output = ()> + 'a {
        let fut = self.close_fut.take();
        self.shutdown_notifier.shutdown();

        async move {
            if let Some(fut) = fut {
                fut.await;
            }
        }
    }
}

impl std::fmt::Debug for CloseHandle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("CloseHandle")
            .field("close_fut", &"Option<Pin<Box<dyn Future<Output = ()>>>>")
            .field("shutdown_notifier", &self.shutdown_notifier)
            .field("actor_id", &self.actor_id)
            .finish()
    }
}

impl<M> Context<M> {
    pub async fn send(&self, message: M) -> Result<(), SendError<M>> {
        self.tx.send(ActorMessage::Msg(message)).await
    }

    pub fn handle(&self) -> SendHandle<M> {
        SendHandle {
            tx: self.tx.clone(),
            weak: self.weak.clone(),
            actor_id: self.actor_id,
        }
    }

    pub fn stop_on_error(&mut self) {
        self.stop_on_error = true;
    }

    pub fn stop(&mut self) {
        self.should_stop = true;
    }

    #[tracing::instrument(skip(self, state, function))]
    pub fn spawn_child<State, F, N>(&mut self, state: State, function: F) -> SendHandle<N>
    where
        for<'a> F: MessageHandler<'a, State, N> + Send + Sync + 'static,
        State: Send + 'static,
        N: Send + 'static,
        M: Send + 'static,
    {
        self.spawn_child_with_hooks(Actor::new(state, function))
    }

    #[tracing::instrument(skip(self, actor))]
    pub fn spawn_child_with_hooks<State, F, Start, Stop, Remove, N>(
        &mut self,
        actor: Actor<State, F, Start, Stop, Remove, N>,
    ) -> SendHandle<N>
    where
        for<'a> F: MessageHandler<'a, State, N> + Send + Sync + 'static,
        for<'a> Start: HookHandler<'a, State, N> + Send + Sync + 'static,
        for<'a> Stop: HookHandler<'a, State, N> + Send + Sync + 'static,
        for<'a> Remove: RemoveHandler<'a, State, N> + Send + Sync + 'static,
        State: Send + 'static,
        N: Send + 'static,
        M: Send + 'static,
    {
        let handle = spawn(actor);
        let actor_id = handle.actor_id;
        let (send_handle, close_handle, close) = handle.split();
        let sender = self.tx.clone();
        tokio::spawn(async move {
            close.await;
            let _ = sender.send(ActorMessage::Closed(actor_id)).await;
        });
        self.children.insert(close_handle.actor_id, close_handle);
        send_handle
    }

    pub fn actor_id(&self) -> usize {
        self.actor_id
    }
}
