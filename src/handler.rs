use crate::{Context, Error};
use std::future::Future;

pub trait MessageHandler<'a, State, Message> {
    type Result: Future<Output = Result<(), Error>> + Send + 'a;

    fn run(
        &mut self,
        state: &'a mut State,
        message: Message,
        context: &'a mut Context<Message>,
    ) -> Self::Result;
}

pub trait HookHandler<'a, State, Message> {
    type Result: Future<Output = Result<(), Error>> + Send + 'a;

    fn run(self, state: &'a mut State, context: &'a mut Context<Message>) -> Self::Result;
}

pub trait RemoveHandler<'a, State, Message> {
    type Result: Future<Output = Result<(), Error>> + Send + 'a;

    fn run(
        &mut self,
        state: &'a mut State,
        child_id: usize,
        context: &'a mut Context<Message>,
    ) -> Self::Result;
}

impl<'a, State, Message, Output, T> MessageHandler<'a, State, Message> for T
where
    T: FnMut(&'a mut State, Message, &'a mut Context<Message>) -> Output + 'static,
    Output: Future<Output = Result<(), Error>> + Send + 'a,
    Message: 'static,
    State: 'static,
{
    type Result = Output;

    fn run(
        &mut self,
        state: &'a mut State,
        message: Message,
        context: &'a mut Context<Message>,
    ) -> Self::Result {
        (self)(state, message, context)
    }
}

impl<'a, State, Message, Output, T> HookHandler<'a, State, Message> for T
where
    T: FnOnce(&'a mut State, &'a mut Context<Message>) -> Output + 'static,
    Output: Future<Output = Result<(), Error>> + Send + 'a,
    Message: 'static,
    State: 'static,
{
    type Result = Output;

    fn run(self, state: &'a mut State, context: &'a mut Context<Message>) -> Self::Result {
        (self)(state, context)
    }
}

impl<'a, State, Message, Output, T> RemoveHandler<'a, State, Message> for T
where
    T: FnMut(&'a mut State, usize, &'a mut Context<Message>) -> Output + 'static,
    Output: Future<Output = Result<(), Error>> + Send + 'a,
    Message: 'static,
    State: 'static,
{
    type Result = Output;

    fn run(
        &mut self,
        state: &'a mut State,
        child_id: usize,
        context: &'a mut Context<Message>,
    ) -> Self::Result {
        (self)(state, child_id, context)
    }
}
