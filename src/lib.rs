mod actor;
mod error;
mod handler;
mod implementation;

pub use actor::Actor;
pub use error::Error;
pub use handler::{HookHandler, MessageHandler, RemoveHandler};
pub use implementation::*;

pub type Result<T> = std::result::Result<T, Error>;
