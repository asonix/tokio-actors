use tracing_error::SpanTrace;

#[derive(Debug)]
pub struct Error {
    inner: Box<dyn std::error::Error + Send>,
    context: SpanTrace,
}

impl Error {
    pub fn source(&self) -> &dyn std::error::Error {
        &*self.inner
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}", self.inner)?;
        std::fmt::Display::fmt(&self.context, f)
    }
}

impl<T> From<T> for Error
where
    T: std::error::Error + Send + 'static,
{
    fn from(error: T) -> Self {
        Error {
            inner: Box::new(error),
            context: SpanTrace::capture(),
        }
    }
}
